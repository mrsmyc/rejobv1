@extends('layouts.app')

@section('title')
    {{$review->title}}
@endsection

@section('content')
    <div class="container">
        <div class="w-50 mx-auto">
            <div>
                <h1 class="">{{$review->title}}</h1>
                <p>{{$review->review_body}}</p>
            </div>
            
            <div>
                <h5>Отзыв на компанию:</h5>
                <p>{{$review->companyBy->company_name}}</p>                
            </div>

            <div>
                <h5>Местоположение компании:</h5>
                <p>{{$review->countryBy->name}}, {{$review->regionBy->name}}, {{$review->cityBy->name}}</p>                
            </div>

            <div>
                <label for="">Автор:</label>                
                <a href="/profile/review/id/{{$review->createdBy->id}}">{{$review->createdBy->name}}</a>
            </div>
            
            @foreach ($images as $image)
                <div class="mt-2">
                    <img src="/uploads/photoes/{{ $image->photo }}" alt="">
                </div>
                
            @endforeach

            @if (Auth::check())
            <div class="mt-2">
                @if (Auth::user()==$review->createdBy)
                    <a class="btn btn-primary" href="{{ route('user_reviews.edit', $review->id )}}">Редактировать</a>
                    <a class="ml-2 btn btn-primary" href="/reviews/delete/{{$review->id}}">Удалить</a>
                @endif
            </div>
            <div class="mt-2">
                @if (Auth::user()->id == $review->createdBy->id)
                   <a class="btn btn-primary" href="/reviews/show/{{$review->id}}/commentsexport">Экспоритировать комментарии</a>
                @endif

            </div>
            @endif
            <h5>Комментарии:</h5>
            @foreach ($comments as $comment)
            
            <div class="mt-3 border-bottom">
                <p>{{$comment->text}}</p>
                <label for="">Автор:</label>
                <a href="/profile/review/id/{{$comment->createdBy->id}}">{{$comment->createdBy->name}}</a>
            </div>

            @endforeach
            <div>
            </div>
            <h5>Оставить комментарий</h5>
            <div class="form-group">
            <form action="/reviews/show/{{$review->id}}" method="POST">
            {{csrf_field()}}
                <textarea class="form-control" name="text" id="text" cols="30" rows="2"></textarea>
                <button class="mt-2 btn btn-primary" type="submit">Отправить</button>
            </form>
            @include('inc.errors')

        </div>
        </div>
    </div>
@endsection