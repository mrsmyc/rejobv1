<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ReviewComment;
use Faker\Generator as Faker;
use App\User;
use App\Review;

$factory->define(ReviewComment::class, function (Faker $faker) {
    return [
        'user_id' => User::all()->random()->id,
        'text' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
        'review_id' => Review::all()->random()->id,
        'created_at' => $faker->dateTimeBetween('-3 months', '-1 months'),
    ];
});
