@extends('layouts.app')

@section('title')
Редактировать профиль
@endsection

@section('content')
    <div class="container">
        <h1 class="">Изменить данные</h1>
        <div class="w-50 mx-auto">
            <form method="POST" action="/profile/redact">
            {{ csrf_field() }}
                <div class="form-group">
                    <label for="name">Имя</label>
                    <input class="form-control" value="{{ Auth::user()->name }}" type="text" id="name" name="name" required>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary mt-3">Сохранить</button>
                </div>
            </form>
        </div>
    </div>
@endsection