<?php

namespace App\Listeners;

use App\Events\SetGlobalVarEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Session;


class SetGlobalVarListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SetGlobalVarEvent  $event
     * @return void
     */
    public function handle(SetGlobalVarEvent $event)
    {
        Session::put('review_sort', 0);
    }
}
