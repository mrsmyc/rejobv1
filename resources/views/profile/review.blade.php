@extends('layouts.app')

@section('title')
Профиль - {{$user->name}}
@endsection

@section('content')
    <div class="container">
        <h1 class="">Профиль пользователя {{$user->name}}</h1>
        <div class="w-50 mx-auto">

            <div class="card align-items-center">
                <div class="">
                    <img class="rounded-circle" src="/uploads/avatars/{{ $user->avatar }}" alt="">
                </div>
                <div class="card-body">
                    <p>{{ $user->name }}</p>
                </div>
                <h4>Отзывы пользователя</h4>
                <div class="mt-3 w-100">
                    @foreach ($reviews as $review)
                        <div class="card mt-3 w-100">
                            <div class="card-body">
                            <h5 class="card-title">{{ $review->title }}</h5>
                            <p class="card-text">{{ $review->review_body }}</p>
                            <a class="mb-1 ml-1 btn btn-primary" href="/reviews/show/{{$review->id}}">Подробнее</a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection