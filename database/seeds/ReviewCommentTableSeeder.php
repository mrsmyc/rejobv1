<?php

use Illuminate\Database\Seeder;

class ReviewCommentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\ReviewComment::class, 100)->create();
    }
}
