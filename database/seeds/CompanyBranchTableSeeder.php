<?php

use Illuminate\Database\Seeder;

class CompanyBranchTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\CompanyBranch::class, 100)->create();
    }
}
