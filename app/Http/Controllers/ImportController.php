<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use App\ReviewComment;
use App\Review;
use App\User;
use App\Country;
use App\Region;
use App\City;
use App\CompanyBranch;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Str;



class ImportController extends Controller
{
    public function importReview(Request $request) {
        
        // $validator = \Validator::make(request()->all(), [
        //     'import_file' => 'required|mimes:xls,xlsx'
        // ]);

        // $validator->after(function ($validator) {
	
        //     if (request('event') == null) {
        //         //add custom error to the Validator
        //         $validator->errors()->add('event', 'Добавьте xls или xlsx файл');
        //     }
        
        // });

        // $validator->validate();



        // $this->validate($request, [
        //     'import_file' => 'required|mimes:xls,xlsx'
        // ]);
        // ,[
        //     'import_file.required' => 'Добавьте xls или xlsx файл'
        // ]
        $inputFileType = 'Xlsx';
        $reader = IOFactory::createReader($inputFileType);
        $path = $request->file('import_file')->getPathName();
        $spreadsheet = $reader->load($request->file('import_file')->getPathName());
        $data = $spreadsheet->getActiveSheet()->toArray();
        foreach($data as $row) {
            $insertData = array(
                ':email'    =>$row[0],
                ':title'    =>$row[1],
                ':body'     =>$row[2],
                ':country'  =>$row[3],
                ':region'   =>$row[4],
                ':city'     =>$row[5],
                ':company'  =>$row[6],
                ':likes'    =>$row[7]
            );
           
            $user = User::where('email',$insertData[':email'])->first();

            if(!$user){

                $newUser = new User();
                $newUser->email = $insertData[':email'];
                $newUser->name = 'inserted';
                $newUser->email_verified_at = Carbon::now();
                $newUser->password = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi';
                $newUser->avatar = 'default.jpg';
                $newUser->remember_token = Str::random(10); 
                $newUser->created_at = Carbon::now();
                $newUser->updated_at = Carbon::now();
                $newUser->save();

                $reviewInsert = new Review();
                $reviewInsert->user_id = $newUser->id;
                $reviewInsert->title = $insertData[':title'];
                $reviewInsert->review_body = $insertData[':body'];
                // $reviewInsert->moderated = 0;
                $reviewInsert->likes_count = $insertData[':likes'];
                $reviewInsert->updated_at = Carbon::now();
                $reviewInsert->created_at = Carbon::now();

                $country = Country::where('name', $insertData[':country'])->first();
                if(!$country)
                {
                    return back()->withErrors([
                        'error' => 'Такой страны - '.$insertData[':country'].' нет.'
                    ]);
                }
                $reviewInsert->country_id = $country->id;

                $region = Region::where('name', $insertData[':region'])->first();
                if(!$region)
                {
                    return back()->withErrors([
                        'error' => 'Такого региона - '.$insertData[':region'].' нет.'
                    ]);
                }
                $reviewInsert->region_id = $region->id;

                $city = City::where('name', $insertData[':city'])->first();
                if(!$city)
                {
                    return back()->withErrors([
                        'error' => 'Такого города - '.$insertData[':city'].' нет.'
                    ]);
                }
                $reviewInsert->city_id = $city->id;

                $company = CompanyBranch::where('company_name',$insertData[':company'])->first();
                if(!$company)
                {
                    return back()->withErrors([
                        'error' => 'Такой компании - '.$insertData[':company'].' нет.'
                    ]);
                }
                $reviewInsert->company_branch_id = $company->id;
                $reviewInsert->save();

            }else {                
                $reviewInsert = new Review();
                $reviewInsert->user_id = $user->id;
                $reviewInsert->title = $insertData[':title'];
                $reviewInsert->review_body = $insertData[':body'];
                // $reviewInsert->moderated = 0;
                $reviewInsert->likes_count = $insertData[':likes'];
                $reviewInsert->updated_at = Carbon::now();
                $reviewInsert->created_at = Carbon::now();

                $country = Country::where('name', $insertData[':country'])->first();
                if(!$country)
                {
                    return back()->withErrors([
                        'error' => 'Такой страны - '.$insertData[':country'].' нет.'
                    ]);
                }
                $reviewInsert->country_id = $country->id;

                $region = Region::where('name', $insertData[':region'])->first();
                if(!$region)
                {
                    return back()->withErrors([
                        'error' => 'Такого региона - '.$insertData[':region'].' нет.'
                    ]);
                }
                $reviewInsert->region_id = $region->id;

                $city = City::where('name', $insertData[':city'])->first();
                if(!$city)
                {
                    return back()->withErrors([
                        'error' => 'Такого города - '.$insertData[':city'].' нет.'
                    ]);
                }
                $reviewInsert->city_id = $city->id;

                $company = CompanyBranch::where('company_name',$insertData[':company'])->first();
                if(!$company)
                {
                    return back()->withErrors([
                        'error' => 'Такой компании - '.$insertData[':company'].' нет.'
                    ]);
                }
                $reviewInsert->company_branch_id = $company->id;
                $reviewInsert->save();
            }

        }

        return redirect()->route('myreviews');




    }
}
