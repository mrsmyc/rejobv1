@extends('layouts.app')

@section('title')
    Редактировать отзыв
@endsection

@section('content')
    <div class="container">
        <h1 class="">Напишите отзыв на компанию</h1>
        <div class="w-50 mx-auto">
        <form method="POST" enctype="multipart/form-data" action="{{ route('user_reviews.update', $review->id) }}">
                @method('PATCH')
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="title">Заголовок</label>
                <input class="form-control" value="{{$review->title}}" type="text" id="title" name="title">
                </div>

                <div class="form-group">
                    <label for="country">Выберите страну:</label>
                    <select class="form-control" name="country" id="country">
                        <option value="{{$review->countryBy->id}}" selected="selected">{{$review->countryBy->name}}</option>
                        @foreach ($countries as $country)
                            @if ($country->id == $review->countryBy->id)
                                @continue
                            @endif
                            <option value="{{$country->id}}">{{$country->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="region">Выберите область:</label>
                    <select class="form-control" name="region" id="region">
                        <option value="{{$review->regionBy->id}}" selected="selected">{{$review->regionBy->name}}</option>
                        @foreach ($regions as $region)
                            @if ($region->id == $review->regionBy->id)
                                @continue
                            @endif
                            <option value="{{$region->id}}">{{$region->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="city">Выберите город:</label>
                    <select class="form-control" name="city" id="city">
                        <option value="{{$review->cityBy->id}}" selected="selected">{{$review->cityBy->name}}</option>
                        @foreach ($cities as $city)
                            @if ($city->id == $review->cityBy->id)
                                @continue
                            @endif
                        <option value="{{$city->id}}">{{$city->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="companies_branches">Выберите компанию:</label>
                    <select class="form-control" name="companies_branches" id="companies_branches">
                        <option value="{{$review->companyBy->id}}" selected="selected">{{$review->companyBy->company_name}}</option>
                        @foreach ($companies_branches as $company_branch)
                            @if ($company_branch->id == $review->companyBy->id)
                                @continue
                            @endif
                            <option value="{{$company_branch->id}}">{{$company_branch->company_name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="body">Отзыв</label>
                    <textarea class="form-control"  name="body" id="body" cols="30" rows="10">{!! $review->review_body!!}</textarea>
                </div>

                <div class="form-group">
                    <label for="review_photo">Фотографии:</label>
                    @foreach ($images as $image)
                    <div class="mt-2">
                        <img src="/uploads/photoes/{{ $image->photo }}" alt="">
                    </div>                
                    @endforeach
                <input class="form-control mt-2" type="file" id="review_photo" name="review_photo[]" value="" multiple>
                </div>

                <div class="form-group my-3">
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                </div>
            @include('inc.errors')
            </form>
        </div>
    </div>
@endsection