<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Review;
use App\ReviewImage;
use Faker\Generator as Faker;

$factory->define(ReviewImage::class, function (Faker $faker) use ($factory) {
    return [
        // 'review_id' => Review::pluck('id')->id,
        'review_id' => $factory->create(App\Review::class)->id,
        'photo' => 'seeder1.webp',
    ];
});
