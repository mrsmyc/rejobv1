<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\CompanyBranch;
use App\City;
use Faker\Generator as Faker;

$factory->define(CompanyBranch::class, function (Faker $faker) {
    return [
        'company_name' => $faker->company,
        'city_id' => City::all()->random()->id,
    ];
});
