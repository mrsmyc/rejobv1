<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Review;
use Faker\Generator as Faker;
use App\User;
use App\Country;
use App\Region;
use App\City;
use App\CompanyBranch;


$factory->define(Review::class, function (Faker $faker) {
    return [
        'user_id' => User::all()->random()->id,
        'title' => $faker->word,
        'review_body' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
        'moderated' => 0,
        'country_id' => Country::all()->random()->id,
        'region_id' => Region::all()->random()->id,
        'city_id' => City::all()->random()->id,
        'company_branch_id' => CompanyBranch::all()->random()->id,
        'created_at' => $faker->dateTimeBetween('-3 months', '-1 months'),
        'likes_count' => $faker->numberBetween($min = 10, $max = 250),
    ];
});
