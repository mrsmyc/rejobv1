@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Отзывы</h1>
        <div class="col-md-3 ml-auto">
            <a href="/reviews/create">Написать отзыв</a>
        </div>
        <div class="col-md-8 mr-auto">
            <div>
                <form action="/reviews/sort/company" method="GET">
                    <div class="form-group">
                        <label for="companies_branches">Выберите компанию:</label>
                        <select class="form-control w-25" name="companies_branches" id="companies_branches">
                            @foreach ($companies_branches as $company_branch)
                            @if ($company_query == $company_branch->id)
                                <option value="{{$company_branch->id}}" selected>{{$company_branch->company_name}}</option>
                            @endif                
                                <option value="{{$company_branch->id}}">{{$company_branch->company_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <button class="btn btn-primary mt-2" type="submit">Найти</button>
                </form>
            </div>
            @include('inc.errors')
            @foreach ($reviews as $review)
                <div class="card mt-3">
                    <div class="card-body">
                        <h5 class="card-title">{{ $review->title }}</h5>
                        <p class="card-text">{{ $review->review_body }}</p>
                        <label for="">Автор:</label>
                        <a href="/profile/review/id/{{$review->createdBy->id}}" class="card-link">{{ $review->createdBy->name }}</a>
                    </div>
                    <div>
                        <a class="mb-1 ml-1 btn btn-primary" href="/reviews/show/{{$review->id}}">Подробнее</a>
                        @if(Auth::check())
                        @if ($review->user_id == Auth::user()->id)
                            <a href="{{ route('user_reviews.edit', $review->id )}}" class="btn btn-primary ml-5 mb-1">Редактировать</a>
                            <a href="/reviews/delete/{{$review->id}}" class="mb-1 btn btn-primary ml-5">Удалить</a>
                        @endif
                        @endif
                    </div>
                    <div class="card-footer">
                        <span>Дата публикации: {{$review->created_at}}</span>
                        <span class="float-right">Понравилось: {{$review->likes_count}}</span>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="mt-3">
            {{ $reviews->appends(request()->toArray())->links() }}

        </div>
    </div>
@endsection