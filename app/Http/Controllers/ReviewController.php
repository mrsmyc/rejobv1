<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Review;
use App\Country;
use App\Region;
use App\City;
use App\CompanyBranch;
use App\ReviewImage;
use Illuminate\Support\Facades\Auth;
use Image;
use Illuminate\Support\Facades\DB;
use App\ReviewComment;
use Illuminate\Database\Eloquent\Builder;

class ReviewController extends Controller
{
    public function __construct() {

        $this->middleware('auth', [
            'except' => [
                'index', 'show', 'sortByCompany', 'search'   
            ]
        ]);
        
    }


    public function index() {
        $companies_branches = CompanyBranch::all();
        $company_query = null;

        return view('reviews.allReviews', ['reviews' => Review::paginate(5)], compact('companies_branches', 'company_query'));
    }



    public function show($id) {

        $review = Review::find($id);
        $images = ReviewImage::where('review_id', $id)->get();
        $comments = ReviewComment::where('review_id', $id)->get() ;
        return view('reviews.show' ,compact('review', 'images', 'comments'));
    }

    public function create() {
        
        $countries = Country::all();
        $regions = Region::all();
        $cities = City::all();
        $companies_branches = CompanyBranch::all();

        return view('reviews.create', compact('countries','regions', 'cities', 'companies_branches'));

    }

    public function store(Request $req) {

        $this->validate(request(), [
            'title' => 'required',
            'review_body' => 'required',
            'country' => 'required|integer',
            'region' => 'required|integer',
            'city' => 'required|integer',
            'companies_branches' => 'required|integer'
        ]);


        $review = new Review();
        $review->user_id = Auth::user()->id;
        $review->title = $req->input('title');
        $review->review_body = $req->input('review_body');
        $review->country_id = $req->country;
        $review->region_id = $req->region;
        $review->city_id = $req->city;
        $review->company_branch_id = $req->companies_branches;
        $review->save();

        $images = $req->file('review_photo');

        if($req->hasFile('review_photo')) {
            //dd($images);
            $imageNameNumber = 0;
            foreach($images as $image) {
                $review_image = new ReviewImage();
                $review_image->review_id = $review->id;
                // $image = $req->file('review_photo');
                $filename = time() . ''.$imageNameNumber.'' . '.' . $image->getClientOriginalExtension();
                Image::make($image)->resize(640, 640)->save( public_path('uploads/photoes/' . $filename));
                $review_image->photo = $filename;
                $review_image->save();
                $imageNameNumber++;
            }            
        }

        return $this->show($review->id);
        // return redirect()->route('home');

    }
    
    public function delete($id) {
        $review = Review::find($id);
        $review->delete();
        return redirect()->route('myreviews');
    }

    public function search(Request $request) {

        $search_query =  $request->input('query');

        $reviews_query = Review::where('title', 'like', "%$search_query%")
            ->orWhere('review_body', 'like', "%$search_query%")
            ->orWhereHas('companyBy', function (Builder $query) use ($search_query) {
                    $query->where('company_name', 'like', "%$search_query%");
            })->orWhereHas('countryBy', function (Builder $query) use ($search_query) {
                    $query->where('name', 'like', "%$search_query%");
            })->orWhereHas('regionBy', function (Builder $query) use ($search_query) {
                    $query->where('name', 'like', "%$search_query%");
            })->orWhereHas('cityBy', function (Builder $query) use ($search_query) {
                    $query->where('name', 'like', "%$search_query%");
            })->orWhereHas('createdBy', function (Builder $query) use ($search_query) {
                    $query->where('name', 'like', "%$search_query%");
            })->get();

        $reviews = $reviews_query->paginate(5);

        return view('search.index', compact('reviews') );
    }

    public function sortByCompany(Request $request) {
        $companies_branches = CompanyBranch::all();
        $company_query = $request->companies_branches;
        $reviews = Review::where('company_branch_id', $company_query)->paginate(5);
        if ($reviews->isEmpty())
        {
            return view('reviews.allReviews', compact('reviews', 'companies_branches', 'company_query'))
            ->withErrors([
                'message' => 'Отзывов на данную компанию не найдено'
            ]);
        }
        return view('reviews.allReviews', compact('reviews', 'companies_branches', 'company_query'));

    }

}
