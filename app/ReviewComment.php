<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReviewComment extends Model
{
    protected $table = 'review_comments';

    public function createdBy() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function review() {
        return $this->belongsTo('App\Review', 'review_id');
    }
    
}
