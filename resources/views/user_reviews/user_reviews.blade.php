@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Мои отзывы</h1>
        <div>
            <a class="btn btn-primary mt-1" href="/myreviews/sort/bydate">Сортировать по дате публикации</a>
        </div>
        <div>
            <a class="btn btn-primary mt-2" href="/myreviews/reviews/export">Экспортировать комментарии к вашим отзывам</a>
        </div>
        <div>
            <form class="" action="/myreviews" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group w-25 mt-5">
                    <input type="file" class="form-control" name="import_file">
                </div>
                <div class="form-group w-25 mt-2">
                    <button class="btn btn-primary form-control" type="submit">Импортировать отзывы</button>
                </div>
            </form>
        </div>
        @include('inc.errors')
        <div class="col-md-3 ml-auto">
            <a href="/reviews/create">Написать отзыв</a>
        </div>
        <div class="col-md-8 mr-auto">
            @foreach ($reviews as $review)
                <div class="card mt-3">
                    <div class="card-body">
                    <h5 class="card-title">{{ $review->title }}</h5>
                    <p class="card-text">{{ $review->review_body }}</p>
                    <label for="">Автор:</label>
                    <a href="/profile/review/id/{{$review->createdBy->id}}">{{  $review->createdBy->name  }}</a>
                    <a class="mb-1 ml-1 btn btn-primary" href="/reviews/show/{{$review->id}}">Подробнее</a>
                    <a href="{{ route('user_reviews.edit', $review->id )}}" class="btn btn-primary ml-5">Редактировать</a>
                    <a href="/myreviews/delete/{{$review->id}}" class="btn btn-primary ml-5">Удалить</a>
                    </div>
                    <div class="card-footer">
                        <span>Дата публикации: {{$review->created_at}}</span>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="mt-3">
            {{ $reviews->appends(request()->toArray())->links() }}
        </div>
    </div>
@endsection