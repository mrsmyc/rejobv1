<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(CountryTableSeeder::class);
        $this->call(RegionTableSeeder::class);
        $this->call(CityTableSeeder::class);
        $this->call(CompanyBranchTableSeeder::class);
        // $this->call(ReviewTableSeeder::class);
        $this->call(ReviewImageTableSeeder::class);
        $this->call(ReviewCommentTableSeeder::class);


    }
}
