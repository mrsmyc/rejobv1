<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\City;
use App\Region;
use Faker\Generator as Faker;

$factory->define(City::class, function (Faker $faker) {
    return [
        'name' => $faker->city,
        'region_id' => Region::all()->random()->id,
    ];
});
