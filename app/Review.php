<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Review extends Model
{
    use SoftDeletes;

    public function createdBy() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function countryBy() {
        return $this->belongsTo('App\Country', 'country_id');
    }

    public function regionBy() {
        return $this->belongsTo('App\Region', 'region_id');
    }

    public function cityBy() {
        return $this->belongsTo('App\City', 'city_id');
    }

    public function companyBy() {
        return $this->belongsTo('App\CompanyBranch', 'company_branch_id');
    }

    public function likes() {
        return $this->hasMany('App\Like');
    }

    protected $table = 'reviews';
}
