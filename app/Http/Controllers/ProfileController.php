<?php

namespace App\Http\Controllers;

use App\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Hash;
use DB;
use Image;

class ProfileController extends Controller
{

    public function __construct() {

        $this->middleware('auth',[
            'except' => [
                'profileReview'
            ] 
        ]);
        
    }


    public function index() {
        return view('profile.index', array('user'=>Auth::user()));
    }

    public function profileReview($id) {
        $user = User::find($id);
        if(!$user){
            return abort(404);
        }
        if($user == Auth::user())
        {
            return view('profile.index', array('user'=>Auth::user()));
        }else {
            $reviews = DB::table('reviews')->where('user_id', '=', $id)->get();
            
            return view('profile.review', compact('user', 'reviews'));
        }

    }

    public function update_avatar(Request $request) {
        if($request->hasFile('avatar')) {
            $avatar = $request->file('avatar');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300, 300)->save( public_path('uploads/avatars/' . $filename) );

            $user = Auth::user();
            $user->avatar = $filename;
            $user->save();
        }

        return view('profile.index', array('user'=>Auth::user()));
    }

    public function edit() {
        return view('profile.edit', array('user'=>Auth::user()));
    }

    public function post(Request $request) {
        $request->validate([
            'name'=>'required'
        ]);
        $user = User::find(Auth::user()->id);
        $user->name = $request->get('name');
        $user->save();
        
        return redirect()->route('myprofile');
    }

    public function editPassword() {
     
        return view('profile.update_password');

    }
    
    public function updatePassword(Request $request) {
        
        $user = Auth::user();

        $old_password = $request->get('old_password');
        $new_password = $request->get('password');
        $new_password_confirm = $request->get('password_confirm');

        if(Hash::check($old_password, $user->password)){
            if(Hash::check($new_password, $user->password)){
                return back()->withErrors([
                    'message' => 'Вы не можете использовать в качестве нового пароля старый!'
                ]);
            }else {
                if($new_password == $new_password_confirm ) {
                    $new_password_hashed = Hash::make($new_password);
                    $user->password = $new_password_hashed;
                    $user->save();
                    return redirect()->route('myprofile')->with('message', 'Пароль изменен!');
                }
                else {
                    return back()->withErrors([
                        'message' => 'Введенные вами пароли различаются'
                    ]);
                }
            }
        }else {
            return back()->withErrors([
                'message' => 'Вы ввели неправильный старый пароль'
            ]);
        }
    }

}
