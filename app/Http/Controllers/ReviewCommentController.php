<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ReviewComment;
use Illuminate\Support\Facades\Auth;

class ReviewCommentController extends Controller
{
    public function store( Request $req, $id) {

        if(Auth::check()) {

            $this->validate(request(), [
                'text' => 'required'
            ],[
                'text.required' => 'Введите комментарий'
            ]);
    
            $comment = new ReviewComment();
            $comment->user_id = Auth::user()->id;
            $comment->text = $req->text;
            $comment->review_id = $id;
            $comment->save();
    
            return back();
        }else {
            return redirect()->route('login');
        }
        
        
    }
}
