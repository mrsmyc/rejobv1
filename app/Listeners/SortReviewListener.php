<?php

namespace App\Listeners;

use App\Events\UserReviewSortEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Session;
class SortReviewListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserReviewSortEvent  $event
     * @return void
     */
    public function handle(UserReviewSortEvent $event)
    {
        Session::put('review_sort', 1);
        // if(!Session::get('review_sort')){
        //     Session::put('review_sort', 1);
        // }elseif(Session::get('review_sort')==1){
        //     Session::put('review_sort', 0);
        // }else{
        //     Session::put('review_sort', 1);
        // }
        
    }
}
