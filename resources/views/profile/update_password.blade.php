@extends('layouts.app')

@section('title')
Обновить пароль
@endsection

@section('content')
    <div class="container">
        <h1 class="">Обновить пароль</h1>
        <div class="w-50 mx-auto">
            <form method="POST" action="/profile/redactpassword">
            {{ csrf_field() }}
                <div class="form-group">
                    <label for="old_password">Введите старый пароль</label>
                    <input class="form-control" value="" type="password" id="old_password" name="old_password" required>
                </div>
                <div class="form-group">
                    <label for="password">Введите новый пароль</label>
                    <input class="form-control" value="" type="password" id="password" name="password" required>
                </div>
                <div class="form-group">
                    <label for="password_confirm">Подтвердите пароль</label>
                    <input class="form-control" value="" type="password" id="password_confirm" name="password_confirm" required>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary mt-3">Сохранить</button>
                </div>
            </form>
            @include('inc.errors')

        </div>

    </div>
@endsection