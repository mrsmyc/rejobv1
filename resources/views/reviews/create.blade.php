@extends('layouts.app')

@section('title')
    Написать отзыв
@endsection

@section('content')
    <div class="container">
        <h1 class="">Напишите свой отзыв компанию</h1>
        <div class="w-50 mx-auto">
            <form method="POST" enctype="multipart/form-data" action="/reviews/create">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="title">Заголовок</label>
                    <input class="form-control" type="text" id="title" name="title" required>
                </div>

                <div class="form-group">
                    <label for="country">Выберите страну:</label>
                    <select class="form-control" name="country" id="country">
                        @foreach ($countries as $country)
                            <option value="{{$country->id}}">{{$country->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="region">Выберите область:</label>
                    <select class="form-control" name="region" id="region">
                        @foreach ($regions as $region)
                            <option value="{{$region->id}}">{{$region->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="city">Выберите город:</label>
                    <select class="form-control" name="city" id="city">
                        @foreach ($cities as $city)
                            <option value="{{$city->id}}">{{$city->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="companies_branches">Выберите компанию:</label>
                    <select class="form-control" name="companies_branches" id="companies_branches">
                        @foreach ($companies_branches as $company_branch)
                            <option value="{{$company_branch->id}}">{{$company_branch->company_name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="review_body">Отзыв:</label>
                    <textarea class="form-control" name="review_body" id="review_body" cols="30" rows="10" required></textarea>
                </div>

                <div class="form-group">
                    <label for="review_photo">Фотографии:</label>
                    <input class="form-control" type="file" id="review_photo" name="review_photo[]" multiple>
                </div>

                <div class="form-group my-3">
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                </div>
            @include('inc.errors')

            </form>
        </div>
    </div>
@endsection