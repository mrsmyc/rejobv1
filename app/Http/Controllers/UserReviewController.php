<?php

namespace App\Http\Controllers;

use App\Review;
use App\UserReview;
use App\ReviewImage;
use App\Country;
use App\Region;
use App\City;
use App\CompanyBranch;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Image;
use App\Events\SetGlobalvarEvent;
use Session;
use App\Events\UserReviewSortEvent;

class UserReviewController extends Controller
{

    public function __construct() {

        $this->middleware('auth');
        
    }

    public function index() {
        $reviews = Auth::user()->index->paginate(5);        
        return view('user_reviews.user_reviews',['reviews' => $reviews] );
    }

    public function sortByDate() {
        $sort_num = Session::get('review_sort');
        if($sort_num == 0){
            $reviews = Auth::user()->index->sortBy('created_at')->paginate(5);
            // $reviews->values()->all();
            event(new UserReviewSortEvent());            
        }else{
            $reviews = Auth::user()->index->sortByDesc('created_at')->paginate(5);
            // $reviews->values()->all()->paginate(5);            
            event(new SetGlobalVarEvent());            
        }
        // dd($reviews);
        return view('user_reviews.user_reviews', compact('reviews'));
    }

    public function edit($id) {
        $review = Review::find($id);
        $images = ReviewImage::where('review_id', $id)->get();
        $countries = Country::all();
        $regions = Region::all();
        $cities = City::all();
        $companies_branches = CompanyBranch::all();
        if(!$review) {
            return abort(404);
        }
        if($review->user_id == Auth::id())
        {
            return view('user_reviews.edit', compact('review', 'images', 'countries', 'regions', 'cities', 'companies_branches'));
        }
        else
        {
            return redirect('myreviews');
        }
        

    }

    public function update(Request $request, $id) {
        $request->validate([
            'title'=>'required',
            'body'=>'required',
            'country' => 'required|integer',
            'region' => 'required|integer',
            'city' => 'required|integer',
            'companies_branches' => 'required|integer'
        ]);
        $review = Review::find($id);
        $review->title = $request->get('title');
        $review->review_body = $request->get('body');
        $review->country_id = $request->country;
        $review->region_id = $request->region;
        $review->city_id = $request->city;
        $review->company_branch_id = $request->companies_branches;
        $review->save();

        $images = $request->file('review_photo');

        if($request->hasFile('review_photo')) {
            $imagesDelete = ReviewImage::where('review_id', $id)->get();
            foreach($imagesDelete as $image) {
                $image->delete();
            }
            $imageNameNumber = 0;
            foreach($images as $image) {
                $review_image = new ReviewImage();
                $review_image->review_id = $review->id;
                // $image = $req->file('review_photo');
                $filename = time() . ''.$imageNameNumber.'' . '.' . $image->getClientOriginalExtension();
                Image::make($image)->resize(640, 640)->save( public_path('uploads/photoes/' . $filename));
                $review_image->photo = $filename;
                $review_image->save();
                $imageNameNumber++;
            }            
        }
        
        return redirect()->route('myreviews');
    }

    public function delete($id) {
        $review = Review::find($id);
        $review->delete();
        return back();
    }
}
