<?php

use App\Http\Controllers\sessionController;
use App\Http\Controllers\UserReviewController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('main');
})->name('home');

Route::get('/register', 'RegistrationController@create');

Route::post('/register','RegistrationController@store');

Route::get('/login', 'SessionController@create')->name('login');

Route::post('/login', 'SessionController@store');

Route::get('/logout', 'SessionController@destroy');

Route::get('/reviews/create', 'ReviewController@create');

Route::post('/reviews/create', 'ReviewController@store');

Route::get('/reviews', 'ReviewController@index');

Route::get('/reviews/delete/{id}', 'ReviewController@delete');

Route::get('/reviews/show/{id}', 'ReviewController@show');

Route::get('/reviews/show/{id}/commentsexport', 'ExportController@commentExport');

Route::get('/myreviews/reviews/export', 'ExportController@reviewsExport');

Route::post('/reviews/show/{id}', 'ReviewCommentController@store');

Route::get('/reviews/sort/company', 'ReviewController@sortByCompany')->name('sortByCompany');

Route::get('/myreviews', 'UserReviewController@index')->name('myreviews');

Route::post('/myreviews', 'ImportController@importReview')->name('importReviews');

Route::get('/myreviews/sort/bydate', 'UserReviewController@sortByDate')->name('sortByDate');

Route::get('/myreviews/delete/{id}', 'UserReviewController@delete');

Route::resource('user_reviews', 'UserReviewController');

Route::get('/profile', 'ProfileController@index');

Route::post('/profile', 'ProfileController@update_avatar')->name('myprofile');

Route::get('/profile/redact', 'ProfileController@edit');

Route::post('/profile/redact', 'ProfileController@post');

Route::get('/profile/redactpassword', 'ProfileController@editPassword');

Route::post('/profile/redactpassword', 'ProfileController@updatePassword');

Route::get('/profile/review/id/{id}', 'ProfileController@profileReview');

Route::get('/reviews/search', 'ReviewController@search')->name('search');






