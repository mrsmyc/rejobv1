<?php

use Illuminate\Database\Seeder;

class ReviewImageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\ReviewImage::class, 100)->create();
    }
}
