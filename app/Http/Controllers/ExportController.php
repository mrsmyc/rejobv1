<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use App\ReviewComment;
use App\Review;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Auth;




class ExportController extends Controller
{
    public function __construct() {

        $this->middleware('auth');
        
    }

    public function commentExport($id) {

        $review = Review::find($id);
        $comments = ReviewComment::where('review_id', $id)->get();
        if($comments->isEmpty()) {
            return back()->withErrors([
                'error' => 'У данного отзыва нет комментариев.'
            ]);
        }

        $spreadsheet = new Spreadsheet();
        // $newWorkShhet = new Worksheet($spreadsheet, 'My Data');
        // $spreadsheet->addSheet($newWorkShhet, 1);
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->mergeCells('A1:C1');
        $sheet->setCellValue('A1', 'Комментарии к отзыву - '.$review->title.'');
        $sheet->setCellValue('A2', 'Имя автора');
        $sheet->setCellValue('B2', 'Текст комментария');
        $sheet->setCellValue('C2', 'Дата публикации');

        $row_id = 3;
        foreach($comments as $comment) {
            unset($comment->id, $comment->review_id, $comment->updated_at);
            $sheet->setCellValue('A'.$row_id, $comment->createdBy->name);
            $sheet->setCellValue('B'.$row_id, $comment->text);
            $sheet->setCellValue('C'.$row_id, $comment->created_at);
            $row_id++;
        }
        $rows_count = $comments->count() + 3;
        $sheet->setCellValue('A'.$rows_count.'', 'Всего комментариев:');
        $sheet->setCellValue('b'.$rows_count.'', $comments->count());
        //<--Style-->//
        
        $styleArray = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                ],
            ],
        ];
        $sheet->getStyle('A1:C1')->getAlignment()->setHorizontal('center');
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $boldCellArray = [
            'font' => [
                'bold' => true,
            ]
        ];
        $sheet->getStyle('A1:C'.$rows_count.'')->applyFromArray($styleArray);
        $sheet->getStyle('A2:C2')->applyFromArray($boldCellArray);
        $sheet->duplicateStyle(
            $sheet->getStyle('A2'),
            'A'.$rows_count.':B'.$rows_count.''
        ); 
        $sheet->getStyle('A1:C1')->applyFromArray($boldCellArray);

        //<--End of Style-->//

        $filename = $review->title . '.' . 'comments' . '.xlsx';

        header('Content-Type: application/vnd.openxmlformats-officedocuments.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');

        $writer = IOFactory::createWriter($spreadsheet, "Xlsx");
        $writer->save('php://output');


    }
    public function reviewsExport() {
        $reviews = Review::where('user_id', Auth::user()->id)->get();
        if($reviews->isEmpty()) {
            return back()->withErrors([
                'error' => 'У вас нет отзывов.'
            ]);
        }
        $spreadsheet = new Spreadsheet();
        $sheetCount = 0;
        $rowId = 3;
        foreach($reviews as $review) {
            $comments = ReviewComment::where('review_id', $review->id)->get();
            $newWorkSheet = new Worksheet($spreadsheet, ''.$review->title.'');
            $spreadsheet->addSheet($newWorkSheet, $sheetCount);
            $newWorkSheet->mergeCells('A1:C1');
            $newWorkSheet->setCellValue('A1', ''.$review->title.'');
            $newWorkSheet->setCellValue('A2', 'Имя автора');
            $newWorkSheet->setCellValue('B2', 'Текст комментария');
            $newWorkSheet->setCellValue('C2', 'Дата публикации');
            foreach($comments as $comment) {
                
                $newWorkSheet->setCellValue('A'.$rowId, $comment->createdBy->name);
                $newWorkSheet->setCellValue('B'.$rowId, $comment->text);
                $newWorkSheet->setCellValue('C'.$rowId, $comment->created_at);
                $rowId++;

            }
            
            $sheetCount++;
            $rowsCount = $comments->count() + 3;
            $newWorkSheet->setCellValue('A'.$rowsCount.'', 'Всего комментариев:');
            $newWorkSheet->setCellValue('b'.$rowsCount.'', $comments->count());
            //<--Style-->//
            
            $styleArray = [
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        'color' => ['argb' => '000000'],
                    ],
                ],
            ];
            $newWorkSheet->getStyle('A1:C1')->getAlignment()->setHorizontal('center');
            $newWorkSheet->getColumnDimension('A')->setAutoSize(true);
            $newWorkSheet->getColumnDimension('B')->setAutoSize(true);
            $newWorkSheet->getColumnDimension('C')->setAutoSize(true);
            $boldCellArray = [
                'font' => [
                    'bold' => true,
                ]
            ];
            $newWorkSheet->getStyle('A1:C'.$rowsCount.'')->applyFromArray($styleArray);
            $newWorkSheet->getStyle('A2:C2')->applyFromArray($boldCellArray);
            $newWorkSheet->duplicateStyle(
                $newWorkSheet->getStyle('A2'),
                'A'.$rowsCount.':B'.$rowsCount.''
            ); 
            $newWorkSheet->getStyle('A1:C1')->applyFromArray($boldCellArray);
            unset($rowsCount);
            unset($rowId);
            
        }
        $sheetIndex = $spreadsheet->getIndex(
            $spreadsheet->getSheetByName('Worksheet')
        );
        $spreadsheet->removeSheetByIndex($sheetIndex);
        $filename = Auth::user()->name . 's' . '.' . 'comments' . '.xlsx';

        header('Content-Type: application/vnd.openxmlformats-officedocuments.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');

        $writer = IOFactory::createWriter($spreadsheet, "Xlsx");
        $writer->save('php://output');

    }
}
