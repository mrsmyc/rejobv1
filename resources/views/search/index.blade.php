
@extends('layouts.app')

@section('title')
Результаты поиска
@endsection

@section('content')
    <div class="container">
        <div class="col-md-8 mr-auto">
            @if(!$reviews->count()==0)
            <h1>Результаты поиска</h1>
            @foreach ($reviews as $review)
                <div class="card mt-3">
                    <div class="card-body">
                        <h5 class="card-title">{{ $review->title }}</h5>
                        <p class="card-text">{{ $review->review_body }}</p>
                        <label for="">Автор:</label>
                        <a href="/profile/review/id/{{$review->createdBy->id}}" class="card-link">{{ $review->createdBy->name }}</a>
                        
                    </div>
                    <div>
                        <a class="mb-1 ml-1 btn btn-primary" href="/reviews/show/{{$review->id}}">Подробнее</a>
                        @if(Auth::check())
                        @if ($review->user_id == Auth::user()->id)
                            <a href="{{ route('user_reviews.edit', $review->id )}}" class="btn btn-primary ml-5 mb-1">Редактировать</a>
                            <a href="/reviews/delete/{{$review->id}}" class="mb-1 btn btn-primary ml-5">Удалить</a>
                        @endif
                        @endif
                    </div>
                </div>
            @endforeach
            <div class="mt-3">
                {{ $reviews->appends(request()->toArray())->links() }}
            </div>
            @else
            <h1>По вашему запросу ничего не найдено, попробуйте заново</h1>
            @endif
        </div>
    </div>
@endsection
